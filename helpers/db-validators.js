const Role = require('../models/role');
const User = require('../models/user');

const isRoleValid = async (rol = ' ') => {
    const existsRole = await Role.findOne({ rol });
    if (!existsRole) {
        throw new Error(`rol ${rol} is invalid or isn't register in DB`);
    }
};

const existsEmail = async (email = ' ') => {
    const existEmail = await User.findOne({ email });
    if (existEmail) {
        throw new Error(`the email ${email} exists in DB`);
    }
};

const existsUserById = async ( id = ' ') => {
    const existUser = await User.findById( id );
    if ( !existUser ) {
        throw new Error(`the id ${id} not exists in DB`);
    }
};


module.exports = {
    isRoleValid,
    existsEmail,
    existsUserById
}