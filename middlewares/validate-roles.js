const { response } = require("express")

const isAdminRol = (req, res = response, next) => {

    if (!req.user) {
        return res.status(500).json({
            msg: 'error when the user rol is verify'
        });
    }

    const { rol, name } = req.user;
    if (rol !== 'ADMIN_ROL') {
        return res.status(401).json({
            msg: `${name} is not admin - action not permitted`
        });
    }
    next();
};

const hasRole = ( ...roles) => {

    return (req, res = response, next) => {
        if (!req.user) {
            return res.status(500).json({
                msg: 'error when the user rol is verify'
            });
        }
        if (!roles.includes(req.user.rol)) {
            return res.status(401).json({
                msg: `is required to have these roles ${ roles}`
            });
        }
        next();
    }
};

module.exports = {
    isAdminRol,
    hasRole
}