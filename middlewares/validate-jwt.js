
const { response } = require('express');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

const validateJWT = async ( req, res = response, next ) => {

    const token = req.header('x-token');

    if (!token) {
        return res.status(401).json({
            msg: 'token is required'
        });
    }

    try {
       const { uid } =  jwt.verify(token, process.env.SECRETORPRIVATEKEY);
        // get user auth
        const user = await User.findById(uid);
        if (!user) {
            return res.status(401).json({
                msg: 'user not exists in DB'
            });
        }
        // check user state
        if (!user.state) {
            return res.status(401).json({
                msg: 'Token invalid'
            });
        }

        req.user = user;
        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg : 'Token invalid'
        });
    }

    console.log(token);
   
};

module.exports = {
    validateJWT
}