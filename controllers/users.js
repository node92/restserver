const { response, request } = require('express');
const bcryptjs = require('bcryptjs');
const User = require('../models/user');

const userGet = async (req, res = response) => {
    
    const { limit = 5, start = 0 } = req.query;
    const query = { state: true }

    const [ total , users ] = await Promise.all([
        User.countDocuments(query),
        User.find(query)
        .skip( Number( start))
        .limit(Number(limit))
    ]);

    res.json({
        total,
        users
    });
}

const userPost = async  (req, res) => {

    const { name , email , password , rol } = req.body;
    const user = new User( { name , email , password , rol } );

    // Encrypt password
    const salt = bcryptjs.genSaltSync(10);
    user.password = bcryptjs.hashSync( password , salt);
    // Save new User
    await user.save();

    res.json({
        user
    });
}

const userPut = async  (req, res) => {

    const id = req.params.id;
    const { password, google,correo, ...userInfo } = req.body;
    
    //TODO: validate with db
    if (password) {
        const salt = bcryptjs.genSaltSync(10);
        userInfo.password = bcryptjs.hashSync( password , salt);
    }

    const user = await User.findByIdAndUpdate(id, userInfo);

    res.json(user);
}

const userDelete = async (req, res) => {
    
    const { id } = req.params;
    const user = await User.findByIdAndUpdate(id, { state: false });

    res.json(user );
}
module.exports = {
    userGet,
    userPost,
    userPut,
    userDelete
}