
const { response } = require('express');
const bcryptjs = require('bcryptjs');

const User = require('../models/user');
const { generateJWT } = require('../helpers/generate-jwt');
const { googleVerify } = require('../helpers/google-verify');

const login = async (req, res = response) => {

    const { email, password } = req.body;

    try {
        //check email
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({
                msg: 'User / Password invalid  - email'
            });
        }
        //check user state
        if (!user.state) {
            return res.status(400).json({
                msg: 'User / Password invalid  - state : false'
            });
        }
        //check password
        const validPassword = bcryptjs.compareSync(password, user.password);
        if (!validPassword) {
            return res.status(400).json({
                msg: 'User / Password invalid  - password'
            });
        }
        //generate JWT
        const token = await generateJWT(user.id);

        res.json({
            user,
            token
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'something went wrong'
        });
    }
}

const googleSingIn = async (req, res = response) => {

    const { id_token } = req.body;
   
    try {
        const { name, img, email } = await googleVerify(id_token);
        let user = await User.findOne({ email });

        if (!user) {
            //create user
            const data = {
                name,
                email,
                password: ' ',
                img,
                google : true
            }
            user = new User(data);
            await user.save();
        }
        // user
        if (!user.state) {
            return res.status(401).json({
                msg: 'user block'
            });
        }
          //generate JWT
        const token = await generateJWT(user.id);
        
        res.json({
            user,
            token,
            msg: "Sing in with google success!"
        });

    } catch (error) {
console.log(error)
        res.status(400).json({
            msg: 'Google token invalid'
           
        });

    }

};

module.exports = {
    login,
    googleSingIn
}