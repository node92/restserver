const express = require('express');
var cors = require('cors')

const { dbConnection } = require('../database/config');
class Server {

    constructor() {

        this.app = express();
        this.port = process.env.PORT;
        this.userRoutePath = '/api/users';
        this.authPath =  '/api/auth';

        // Coneect Database
        this.connectDB();
        // Middlewares
        this.middlewares();
        // Rutas de la App
        this.routes();
    }

    async connectDB() {
        await dbConnection()
    }

    middlewares() {

        //CORS
        this.app.use(cors());
        // Lectura y Parse del Body
        this.app.use(express.json());
        //Directorio publico
        this.app.use(express.static('public'));

    }

    routes() {
        this.app.use(this.authPath, require('../routes/auth'));
        this.app.use(this.userRoutePath, require('../routes/user'));

    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Server running on port', this.port)
        });

    }

}

module.exports = Server;