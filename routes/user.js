const { Router } = require('express');
const { check } = require('express-validator');

const { isRoleValid, existsEmail, existsUserById } = require('../helpers/db-validators');
const {
  validateFields,
  validateJWT,
  isAdminRol,
  hasRole
} = require('../middlewares');

const {
  userGet,
  userPost,
  userPut,
  userDelete
} = require('../controllers/users');

const router = Router();

router.get('/', userGet);

router.put('/:id', [
  check('id', 'invalid ID').isMongoId(),
  check('id').custom(existsUserById),
  check('rol').custom(isRoleValid),
  validateFields
], userPut);

router.post('/', [
  check('name', 'name is required').not().isEmpty(),
  check('password', 'password is required').isLength({ min: 6 }),
  check('email', 'email is invalid').isEmail(),
  check('email').custom(existsEmail),
  //  check('rol', 'rol is invalid').isIn(['ADMIN_ROLE', 'USER_ROLE']),
  check('rol').custom(isRoleValid),
  validateFields
], userPost);

router.delete('/:id', [
  validateJWT,
  // isAdminRol,
  hasRole('ADMIN_ROLE', 'VENTAS_ROLE'),
  check('id', 'invalid ID').isMongoId(),
  check('id').custom(existsUserById),
  validateFields
], userDelete);

module.exports = router;